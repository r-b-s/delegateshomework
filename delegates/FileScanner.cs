﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace delegates
{
    public class FileArgs : EventArgs
    {
        public readonly string _fileName;
        public readonly long _size;
        public FileArgs(string fileName, long size)
        {
            _fileName = fileName;
            _size = size;
        }
    }
    public  class FileScanner
    {
        private bool CancelToken  = false;

        public event EventHandler<FileArgs> FileFound;
       

        public void Cancel()
        {
            CancelToken = true;
        }
        public void ScanDir(DirectoryInfo dir,ref List<FileArgs> arr)
        {
            
            foreach (var f in dir.GetFiles())
            {
                FileArgs n= new FileArgs(f.Name, f.Length);
                arr.Add(n);
                FileFound(this,n );                
                if (CancelToken)
                    {
                        Console.WriteLine("Canceled!");
                        CancelToken = false;
                        break;
                    }                
            }
        }


    }
}
