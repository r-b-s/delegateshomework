﻿
using System.Collections;


namespace delegates
{
    public static class Program
    {

        public static T GetMax<T>(this IEnumerable e, Func<T, float> getParameter) where T : class
        {
            T max = null;
            float maxFl = 0;

            foreach (T elm in e)
            {
                if (getParameter(elm) > maxFl)
                {
                    maxFl = getParameter(elm);
                    max = elm;
                }
            }
            return max;
        }

        public static float getParameter(FileArgs file)
        {
            return (float)file._size;
        }


        static void Main(string[] args)
        {
            

            

            List<FileArgs> FileArray = new List<FileArgs> {  };

            FileScanner fs = new FileScanner();
            System.Timers.Timer timer = new System.Timers.Timer(500);
            timer.Elapsed += OnTimedEvent;
            timer.Start();

            void GotFile(object sender, FileArgs f)
            {
                Console.WriteLine($"{f._fileName}");
                var m = FileArray.GetMax<FileArgs>(getParameter);
                Console.WriteLine($"MaxSize ={m._size} ({m._fileName})");
            }

            fs.FileFound += GotFile;
            fs.ScanDir(new DirectoryInfo("C:\\Windows\\System32"),ref  FileArray);         
                      
            Console.ReadKey();

            void OnTimedEvent(Object source, System.Timers.ElapsedEventArgs e)
            {
                fs.Cancel();
            }


        }

  
        
    }
}